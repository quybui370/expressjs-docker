class BaseService {
  constructor(model) {
    this.model = model;
  }

  create = (params) => {
    let entity = this.model.instance(params);
    return entity.save();
  };

  find = (query) => {
    return this.model.instance.find(query);
  };

  findOne = (query) => {
    return this.model.instance.findOne(query);
  };

  update = (query, value) => {
    return this.model.instance.findOneAndUpdate(query, value, {
      new: true,
      runValidators: true,
    });
  };

  delete = (query) => {
    return this.model.instance.findOneAndRemove(query);
  };
}

module.exports = BaseService;