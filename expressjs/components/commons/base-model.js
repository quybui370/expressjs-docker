let mongoose = require('mongoose');

class BaseModel {

  constructor(name, schema) {
    this.name = name;
    this.schema = new mongoose.Schema(schema);
    this.model = mongoose.model(this.name, this.schema);
  }

  get instance() {
    return this.model;
  }
}

module.exports = BaseModel;