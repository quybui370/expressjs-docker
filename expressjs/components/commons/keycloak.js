const Keycloak = require('keycloak-connect');
const config = require('#config');

module.exports = new Keycloak({}, config.keycloak);