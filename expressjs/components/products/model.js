let BaseModel = require('#commons/base-model');

class ProductModel extends BaseModel {
  constructor() {
    let name = "Product";
    let schema = {
      name: String,
      category: String,
      price: String,
    };
    super(name, schema);
  }
}

module.exports = new ProductModel();