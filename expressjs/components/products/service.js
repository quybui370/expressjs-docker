let productModel = require('./model');
let BaseService = require('#commons/base-service');

class ProductService extends BaseService {
  constructor() {
    super(productModel);
  }
}

module.exports = new ProductService();