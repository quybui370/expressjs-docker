const express = require('express');
const router = express.Router();
const services = require('./service');
const keycloak = require('#commons/keycloak');

router.get('/', function(req, res) {
  res.render('./products/view', {title: '200 OK'});
});

router.get('/list', keycloak.enforcer(['resource:view', 'resource:write'], {
  resource_server_id: 'nodejs-apiserver'
}), function (req, res) {
  services.find({})
    .then(data => {
      res.json({
        success: true,
        data
      });
    })
    .catch(err => {
      res.json({
        success: false,
        message: err
      });
    });
});

router.get('/:id/details', keycloak.enforcer(['resource:view', 'resource:write'], {
  resource_server_id: 'nodejs-apiserver'
}), function (req, res) {
  services.findOne({_id: req.params.id})
    .then(data => {
      res.json({
        success: true,
        data
      });
    })
    .catch(err => {
      res.json({
        success: false,
        message: err
      });
    });
});

router.post('/new', keycloak.enforcer(['resource:view', 'resource:write'], {
  resource_server_id: 'nodejs-apiserver'
}), function (req, res) {
  services.create(req.body)
    .then(data => {
      res.json({
        success: true,
        data
      });
    })
    .catch(err => {
      res.json({
        success: false,
        message: err
      });
    });
});

router.put('/:id', keycloak.enforcer(['resource:view', 'resource:write'], {
  resource_server_id: 'nodejs-apiserver'
}), function (req, res) {
  services.update({_id: req.params.id}, req.body)
    .then(data => {
      res.json({
        success: true,
        data
      });
    })
    .catch(err => {
      res.json({
        success: false,
        message: err
      });
    });
});

router.delete('/:id', keycloak.enforcer(['resource:view', 'resource:write'], {
  resource_server_id: 'nodejs-apiserver'
}), function (req, res) {
  services.delete({_id: req.params.id})
    .then(data => {
      res.json({
        success: true,
        data
      });
    })
    .catch(err => {
      res.json({
        success: false,
        message: err
      });
    });
});

module.exports = router;
