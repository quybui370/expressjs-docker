let BaseModel = require('#commons/base-model');

class UserModel extends BaseModel {
  constructor() {
    let name = "User";
    let schema = {
      firstname: String,
      lastname: String,
      username: String,
      password: String,
    };
    super(name, schema);
  }
}

module.exports = new UserModel();