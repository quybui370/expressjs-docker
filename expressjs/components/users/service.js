let jwt = require('jsonwebtoken');
let config = require('#config');
let userModel = require('./model');
let ultis = require('#ultis');
let BaseService = require('#commons/base-service');

class UserService extends BaseService {
  constructor() {
    super(userModel);
  }

  login(username, password) {
    const hashedPassword = ultis.createHash(password);
    return new Promise((resolve, reject) => {
      this.findOne({username, password: hashedPassword})
        .then(user => {
          if (user.username && user.password) {
            let token = jwt.sign({username, hashedPassword},
              config.jwtSecret,
              {
                expiresIn: '24h' // expires in 24 hours
              }
            );
            // return the JWT token for the future API calls
            resolve({
              success: true,
              message: 'Authentication successful!',
              token: token
            });
          } else {
            reject({
              success: false,
              message: 'Authentication failed! Please check the request'
            });
          }
        })
        .catch(() => {
          reject({
            success: false,
            message: 'Incorrect username or password'
          });
        });
    });
  }

  signup(firstname, lastname, username, password) {
    const hashedPassword = ultis.createHash(password);
    return new Promise((resolve, reject) => {
      this.create({
        firstname,
        lastname,
        username,
        password: hashedPassword,
      }).then(user => {
        resolve({
          success: true,
          message: 'Create new account successful!',
        });
      }).catch(() => {
        reject({
          success: false,
          message: 'Cannot create a new account.'
        });
      });
    });
  }
}

module.exports = new UserService();
