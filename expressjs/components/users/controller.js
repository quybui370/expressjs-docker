var express = require('express');
var router = express.Router();
var services = require('./service');

router.get('/', (req, res) => {
  res.render('./users/view', {title: '200 OK'});
});

router.post('/jwt/login', (req, res) => {
  services.login(req.body.username, req.body.password)
    .then(data => {
      res.status(200);
      res.json(data);
    })
    .catch(err => {
      res.status(400);
      res.json(err);
    });
});

router.post('/jwt/signup', (req, res) => {
  const { firstname, lastname, username, password } = req.body;
  services.signup(firstname, lastname, username, password)
    .then(data => {
      res.status(200);
      res.json(data);
    })
    .catch(err => {
      res.status(200);
      res.json(err);
    });
});

module.exports = router;
