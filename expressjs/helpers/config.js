module.exports = {
  port: process.env.PORT || 3000,
  env: process.env.NODE_ENV || "development",
  jwtSecret: 'secret',
  development: {
    db: {
      server: "mongo:27017",
      name: "expressjs",
      user: "root",
      password: "hCcY+h=5gvdN3W#T",
      connectionString: "mongodb://root:hCcY+h=5gvdN3W#T@mongo:27017/expressjs?authSource=admin",
    }
  },
  production: {
    db: {
      server: "mongo:27017",
      name: "expressjs",
      user: "root",
      password: "hCcY+h=5gvdN3W#T",
      connectionString: "mongodb://root:hCcY+h=5gvdN3W#T@mongo:27017/expressjs?authSource=admin",
    }
  },
  keycloak: {
    "realm": "nodejs-keycloak",
    "auth-server-url": "http://keycloak:8080/auth",
    "ssl-required": "external",
    "resource": "nodejs-apiserver",
    "verify-token-audience": true,
    "credentials": {
      "secret": "30a388bb-e214-483b-adb0-d4233b62b18d"
    },
    "confidential-port": 0,
    "policy-enforcer": {}
  },
};
