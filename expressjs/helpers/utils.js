var crypto = require('crypto');

class Ultis {
  static createHash(data) {
    return crypto.createHash('md5').update(data).digest("hex");
  }
}

module.exports = Ultis;