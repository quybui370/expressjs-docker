var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const userRouter = require('./components/users/controller');
const productRouter = require('./components/products/controller');
const db = require('./database/connection');
const appMiddlewares = require('./components/commons/middlewares');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'components'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(appMiddlewares.cors);

app.use('/api/user', userRouter);
app.use('/api/product', productRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // next(createError(404));
  res.status = 404;
  res.json({
    success: false,
    message: 'API not found'
  })
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('./commons/error');
});

module.exports = app;
