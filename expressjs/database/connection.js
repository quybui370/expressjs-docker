let mongoose = require('mongoose');
let config = require('#config');

const db = mongoose.connect(config[config.env].db.connectionString, {useNewUrlParser: true, useUnifiedTopology: true}).then(
  () => {
    console.log('MongoDB is connected !!!');
    return mongoose;
  },
  (error) => {
    console.log(`Connection error: ${error}`);
    return null;
  }
);

module.exports = db;