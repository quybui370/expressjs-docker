module.exports = {
  async up(db, client) {
    return db.collection('products').insertMany([
      {name: 'iPhone SE 64GB (2020)', category: 'Smart Phone', price: '11.990.000 ₫'},
      {name: 'Samsung Galaxy Note 10 Lite', category: 'Smart Phone', price: '8.790.000 ₫'},
      {name: 'iPad 10.2 Inch WiFi 32GB New 2019', category: 'Tablet', price: '8.750.000 ₫'},
      {name: 'Samsung Galaxy Tab A8', category: 'Tablet', price: '3.550.000 ₫'},
    ]);
  },

  async down(db, client) {
    await db.collection('products').deleteMany({ category: 'Smart Phone' });
    await db.collection('products').deleteMany({ category: 'Tablet' });
  }
};
