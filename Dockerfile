FROM node:latest
WORKDIR /home/node/app
COPY package*.json ./
RUN npm install
COPY ./expressjs .
COPY ./node_modules .
COPY .env .
EXPOSE 80
CMD [ "npm", "run", "debug" ]
